import {BrowserRouter, Route, Routes} from 'react-router-dom';
//import {useLocation} from 'react-router-dom';
import NavBar from './components/navbar';
import List from './components/list';
import Detail from './components/detail';
import Gallery from './components/gallery';

import './App.css';
//import { useEffect } from 'react';

function App() {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="App">
        <NavBar navTitle="PokeAPI"/>
        <Routes>
          <Route path="/" exact element={<List />}/>
          <Route path="/list" element={<List />}/>
          <Route path="/gallery" element={<Gallery />}/>
          <Route path="/detail" element={<Detail />}/>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
