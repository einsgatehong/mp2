import React from 'react';
import PropTypes from 'prop-types';

function listItem({
  content,
  func
}) {
  const rows = [];
  content.forEach(character => {
    const item =
      <tr onClick={() => { func(character[0]) }}>
        <th>{character[0]}</th>
        <th>{character[1]}</th>
        <th>{character[2]}</th>
        <th>{character[3]}</th>
        <th>{character[4]}</th>
      </tr>;
    rows.push(item)
  });
  return (
    <table id="result-table">
      <tbody>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Date</th>
          <th>Artist</th>
          <th>Artwork</th>
        </tr>
        {rows}
      </tbody>
    </table>
  );
}

listItem.proptype = {
  content: PropTypes.array.isRequired
}


export default listItem;
