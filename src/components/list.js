import React from 'react';
import { createRoot } from 'react-dom/client';
import LinkedTable from './listItem';

// import {Link} from 'react-router-dom';
import Header from './Header';
import Axios from 'axios';
import { useNavigate } from "react-router-dom";


function List() {
  const naviTo = useNavigate();
  const jumpTo = (num) => {
    naviTo("/detail#" + num);
  }
  const search = async () => {
    const dataHolder = document.getElementById('data-holder');
    const textBoxValue = document.getElementById("search-bar").value;
    const sorting = document.getElementById("sort").value;
    dataHolder.innerHTML = "Search Results";

    const result = await Axios.get('https://api.artic.edu/api/v1/artworks/?limit=100');
    let nameList = [];
    result.data.data.forEach(artwork => {

      const artwork_img = <img src={'https://www.artic.edu/iiif/2/' + artwork.image_id + '/full/843,/0/default.jpg'} width="100px" alt="no images" ></img>
      const content = [artwork.id, artwork.title, artwork.date_end, artwork.artist_titles, artwork_img];

      nameList.push(content);
    });

    if (sorting === "id-ascending") {
      nameList.sort((a, b) => a[0] - b[0])
    } else if (sorting === "id-descending") {
      nameList.sort((a, b) => b[0] - a[0])
    } else if (sorting === "date-ascending") {
      nameList.sort((a, b) => a[2] - b[2])
    } else if (sorting === "date-descending") {
      nameList.sort((a, b) => b[2] - a[2])
    }

    let searchResults = [];
    for (let i = 0; i < nameList.length; i++) {
      if (nameList[i][1].includes(textBoxValue)) searchResults.push(nameList[i]);
    }
    const linkedTable = <LinkedTable content={searchResults} func={jumpTo} />;
    const root = createRoot(dataHolder);
    root.render(linkedTable);
  };
  React.useEffect(() => { search(); });
  return (
    <div id='list-container'>
      <Header pageType="List"/>
      <h3>The list view will provide id, title, date and artist of Art works! Please enjoy it!</h3>
      <div id="search-container">
        {/* <label for="search-bar" className="text">Search: </label> */}
        <input type="text" id="search-bar" placeholder='Search by Name'></input>
        <label for="sort" className="text">Sort: </label>
        <select id="sort">
          <option value="id-ascending">ID Ascending</option>
          <option value="id-descending">ID Descending</option>
          <option value="date-ascending">Date Ascending</option>
          <option value="date-descending">Date Descending</option>
        </select>
        <button onClick={search}>Search</button>
      </div>
      <div id="data-holder">
      </div>
    </div>
  );
}

export default List;