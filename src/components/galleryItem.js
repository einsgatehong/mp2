import React from 'react';
import PropTypes from 'prop-types';

import './galleryitem.css';

function galleryitem({
  id,
  imageURL,
  func
}) {
  return (
    <div className='gallery-item' onClick={() => {func(id);}}>
      <img src={imageURL} alt="img"/>
    </div>
  );
}

galleryitem.proptype = {
  id : PropTypes.number.isRequired,
  name : PropTypes.string.isRequired,
  imageURL : PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
  func : PropTypes.func.isRequired
}

export default galleryitem;