import React from 'react'
import Axios from 'axios';
import { useLocation } from 'react-router-dom';
import { createRoot } from 'react-dom/client';
import { useNavigate } from "react-router-dom";
import './detail.css'

export default function Detail() {
  const naviTo = useNavigate();
  const jumpTo = (num) => {
    naviTo("/detail#" + num);
  }

  const pageURL = useLocation();
  let contents;
  const fetchAPI = async (id) => {
    const results = await Axios.get('https://api.artic.edu/api/v1/artworks/' + id);
    const result = results.data.data;
    console.clear();
    console.log(result);
    contents =
      <div id="info-container">
        <table id="detail-table">
          <tr>
            <th>ID</th>
            <th>{result.id}</th>
          </tr>
          <tr>
            <th>Title</th>
            <th>{result.title}</th>
          </tr>
          <tr>
            <th>Author</th>
            <th>{result.artist_titles}</th>
          </tr>
          <tr>
            <th>Artwork</th>
            <th>
              <img src={'https://www.artic.edu/iiif/2/' + result.image_id + '/full/843,/0/default.jpg'} width="300px" alt="unknown" />
            </th>
          </tr>
          <tr>
            <th>Place</th>
            <th>{result.place_of_origin}</th>
          </tr>

          <tr>
            <th>Type</th>
            <th>{result.artwork_type_title}</th>
          </tr>
          <tr>
            <th>Date</th>
            <th>{result.date_end}</th>
          </tr>
        </table>
      </div>;
  };

  const prevPage = () => {
    jumpTo(parseInt(pageURL.hash.replace('#', '')) - 1);
  }
  const nextPage = () => {
    jumpTo(parseInt(pageURL.hash.replace('#', '')) + 1);
  }

  React.useEffect(() => {

    if (pageURL.hash !== '') {
      fetchAPI(pageURL.hash.replace('#', ''));
      const dataHolder = createRoot(document.getElementById("details"));
      setTimeout(() => { dataHolder.render(contents); }, 300);
    }
  });

  return (
    <div id='detail-container'>
      <h2>Detail Page</h2>
      <h3 className='tips'>If you cannot see the details, please refresh page!</h3>
      <h4 className='tips'>If still not work, go to next page!</h4>
      <div id="details"></div>
      <button onClick={prevPage}>Previous</button>
      <button onClick={nextPage}>Next</button>
    </div>
  );
}
