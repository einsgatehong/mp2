import React from 'react';
import art from './art.jpg';
import './Header.css';
export default function Header(props) {
  
  return (
    <div className='header'>
      <h1 className='title'>Art Institute of Chicago</h1>
      <img className='header-img' src={art} alt='art'/>
      <h3>Welcome to {props.pageType} Page!</h3>
    </div>
  )
}
