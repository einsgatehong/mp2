import React from 'react';
import {Link} from 'react-router-dom';

import './navbar.css';

export default function navbar() {
  return (
    <div id='nav-bar-container'>
      <Link className='nav-bar-button' to='/list' id='list'>List</Link>
      <Link className='nav-bar-button' to='/gallery' id='gallery'>Gallery</Link>
    </div>
  )
}


