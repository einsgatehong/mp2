import React from 'react';
import Axios from 'axios';
import { createRoot } from 'react-dom/client';
import GalleryItem from './galleryItem';
import { useNavigate } from "react-router-dom";
import Header from './Header';

function App() {
  const naviTo = useNavigate();
  const jumpTo = (num) => {
    naviTo("/detail#" + num);
  }
  const search = async () => {
    const dataHolder = document.getElementById('gallery-items-container');
    const filter = document.getElementById('filter-selection').value;
    let cards = [];
    const ids=[20440,27954,28560,37052,37057,37072,37089,37113,37116,37138,37141,37151,37155,37323,43813,48720,48723,48726,48731,48734,48738,54201,64957,66042,69102,69105,69108,69112,69114]
    for (const id of ids) {
      const results = await Axios.get('https://api.artic.edu/api/v1/artworks/' + id);
      console.clear()
      const result = results.data.data;
      let item = <GalleryItem
        id={result.id}
        name={result.title}
        imageURL={'https://www.artic.edu/iiif/2/' + result.image_id + '/full/843,/0/default.jpg'}
        date={result.date_end}
        // moves={result.data.moves.length}
        // height={result.data.height}
        // weight={result.data.weight}
        func={jumpTo} />;
      console.log(item.id)
      switch (filter) {
        case "ancient":
          if (result.date_end <= 0) {
            cards.push(item);
          }
          break;
        case "midage":
          if (result.date_end> 0 && result.date_end<= 1800) {
            cards.push(item);
          }
          break;
        case "modern":
          if (result.date_end> 1800 && result.date_end<= 1900) {
            cards.push(item);
          }
          break;
        case "contemporary":
          if (result.date_end> 1900) {
            cards.push(item);
          }
          break;
        default:
          cards.push(item);
      }
    }

    console.log(cards);

    const root = createRoot(dataHolder);
    root.render(cards);
  };
  React.useEffect(() => {
    search();
  });

  return (
    <div id='gallery-container'>
      <Header pageType="Gallery"/>
      <label for="filter">Select artworks you are interested by time: </label>
      <select name="filter" id="filter-selection">
        <option value="none">None</option>
        <option value="ancient">Ancient:before Jesus</option>
        <option value="midage">Midage:0-1800</option>
        <option value="modern"> Modern:1800-1900</option>
        <option value="contemporary">Contemporary:After 1900:</option>
      </select>
      <button onClick={search}>Search</button>
      <div id="gallery-items-container">
        <h1>Gallery is coming!!!Wait 5 seconds!</h1>
      </div>
    </div>
  );
}

export default App;
